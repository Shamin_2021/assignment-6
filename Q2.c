#include<stdio.h>


int printFibonacci(int n){    
    if(n==0){
      return 0;
    }
    else if (n==1){
      return 1;
    } 
    else{
      return printFibonacci(n-1)+printFibonacci(n-2);
    }  
}

int main(){    
    int row;    
    printf("Enter the number of rows for the fibonacci sequence: ");  
    scanf("%d",&row);    
    printf("Fibonacci Series: ");    
    for(int i=0;i<=row;i++){
      printf("%d \n",printFibonacci(i));
    }    
  return 0;  
 }    